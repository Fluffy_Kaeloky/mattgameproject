﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnSpawnEnemyEventArgs
{
    private UISpawnQueueEnemy sender = null;
    public UISpawnQueueEnemy Sender { get { return sender; } }

    public OnSpawnEnemyEventArgs(UISpawnQueueEnemy sender)
    {
        this.sender = sender;
    }
}
[System.Serializable]
public class OnSpawnEnemyEvent : UnityEvent<OnSpawnEnemyEventArgs> { }

[System.Serializable]
public class OnDespawnEnemyEventArgs
{
    private Entity target = null;
    public Entity Target { get { return target; } }

    public OnDespawnEnemyEventArgs(Entity target)
    {
        this.target = target;
    }
}
[System.Serializable]
public class OnDespawnEnemyEvent : UnityEvent<OnDespawnEnemyEventArgs> { }