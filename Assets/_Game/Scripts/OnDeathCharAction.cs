﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BaseCharInput))]
[RequireComponent(typeof(CharAnimationController))]
public class OnDeathCharAction : MonoBehaviour
{
    private CharAnimationController charAnimationController = null;
    private BaseCharInput baseCharInput = null;

    private void Start()
    {
        charAnimationController = GetComponent<CharAnimationController>();
        baseCharInput = GetComponent<BaseCharInput>();
    }

    public void OnDeath(OnDeathArgs args)
    {
        charAnimationController.Kill();
        baseCharInput.enabled = false;
    }
}
