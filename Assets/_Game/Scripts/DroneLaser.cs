﻿using UnityEngine;
using System.Collections;
using System;

public class DroneLaser : MonoBehaviour
{
    public LineRenderer lineRenderer = null;

    public string targetLayer = "PJ";

    public float fireTime = 1.5f;
    public float lifetime = 3.0f;

    public float damages = 10.0f;

    public Transform owner = null;

    public Vector3 Direction { get { return direction; } set { direction = /*transform.InverseTransformDirection(*/value.normalized; } }
    private Vector3 direction = Vector3.zero;

    private Vector3 endpoint = Vector3.zero;

    #region Public Methods

    #endregion

    #region Private Methods

    private void Start()
    {
        if (lineRenderer != null)
            lineRenderer.useWorldSpace = true;

        StartCoroutine(FireCoroutine());

        Destroy(gameObject, lifetime);
    }

    private void Update()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, direction);

        if (Physics.Raycast(ray, out hit))
            endpoint = hit.point;
        else
            endpoint = transform.position + direction * 100.0f;

        if (lineRenderer != null)
        {
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, endpoint);
        }
    }

    private IEnumerator FireCoroutine()
    {
        yield return new WaitForSeconds(fireTime);

        RaycastHit[] hits;
        Ray ray = new Ray(transform.position, direction);

        LayerMask mask = LayerMask.NameToLayer(targetLayer);
        mask = ~mask;
        hits = Physics.RaycastAll(ray, endpoint.magnitude, mask.value, QueryTriggerInteraction.Ignore);

        RaycastHitDistanceComparer distanceComparer = new RaycastHitDistanceComparer();

        Array.Sort(hits, distanceComparer);

        Debug.DrawLine(transform.position, transform.position + ray.direction * endpoint.magnitude, Color.red);

        if (hits.Length == 0)
            yield return null;

        foreach (RaycastHit hit in hits)
        {
            if (hit.transform.IsChildOf(transform) || (owner != null && hit.transform.IsChildOf(owner)))
                continue;

            Entity ent = hit.transform.GetComponentInParent<Entity>();
            if (ent != null)
            {
                LifeManager lifeManager = ent.GetComponent<LifeManager>();
                if (lifeManager != null)
                    lifeManager.TakeDamage(damages);
            }
        }
    }

    #endregion
}
