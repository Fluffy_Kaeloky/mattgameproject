﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILifeBar : MonoBehaviour
{
    [SerializeField]
    private LifeManager target = null;

    [SerializeField]
    private RectTransform fill = null;

    private void Start()
    {
        target.onLifeChanged.AddListener(OnLifeChanged);
    }

    private void OnLifeChanged(OnLifeChangedArgs args)
    {
        fill.localScale = new Vector3(args.NewAmount / args.Sender.MaxLife, 1.0f, 1.0f); 
    }
}
