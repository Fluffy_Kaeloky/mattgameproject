﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UISpawnQueueEnemy : MonoBehaviour
{
    public Image profileSprite = null;
    public Image buttonSprite = null;
    public SpawnQueue.SpawnButtonSpriteInfo[] buttonSpriteInfos = new SpawnQueue.SpawnButtonSpriteInfo[0];

    [System.Serializable]
    public class OnSpawnQueueEmptyDestroyEventArgs
    {
        private UISpawnQueueEnemy sender = null;
        public UISpawnQueueEnemy Sender { get { return sender; } }

        public OnSpawnQueueEmptyDestroyEventArgs(UISpawnQueueEnemy sender)
        {
            this.sender = sender;
        }
    }
    public class OnSpawnQueueEmptyDestroyEvent : UnityEvent<OnSpawnQueueEmptyDestroyEventArgs> { }
    public OnSpawnQueueEmptyDestroyEvent onDestroy = new OnSpawnQueueEmptyDestroyEvent();

    private EnemyInfo enemyInfo;
    public EnemyInfo EnemyInfo { get { return enemyInfo; } }
    private SpawnQueue.SpawnButtonCode spawnButton;
    public SpawnQueue.SpawnButtonCode SpawnButton { get { return spawnButton; } }

    public void SetEnemyInfo(EnemyInfo info)
    {
        enemyInfo = info;
        profileSprite.sprite = enemyInfo.image;
    }

    public void SetSpawnKey(SpawnQueue.SpawnButtonCode keyCode)
    {
        spawnButton = keyCode;
        buttonSprite.sprite = buttonSpriteInfos.FirstOrDefault(x => x.code == keyCode).image;
    }

    public void OnSpawnButtonEvent(SpawnQueue.SpawnButtonPressedRedistrbutedEvent e)
    {
        if (e.KeyCode == spawnButton)
        {
            SpawnEnemy(e.InputPrefix);

            e.Absorb();

            if (onDestroy != null)
                onDestroy.Invoke(new OnSpawnQueueEmptyDestroyEventArgs(this));

            Destroy(gameObject);
        }
    }

    private void SpawnEnemy(string prefix)
    {
        enemyInfo.entity.transform.position = BattleDirector.Instance.GetRandomSpawn().position;
        enemyInfo.entity.gameObject.SetActive(true);

        PlayerCharInput inputSystem = enemyInfo.entity.GetComponent<PlayerCharInput>();
        if (inputSystem != null)
            inputSystem.inputControllerPrefix = prefix;
    }
}
