﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIInputJoystickManager : MonoBehaviour
{
    public PlayerCharInput target = null;

    public void OnValueChanged(int value)
    {
        string prefix = "";
        switch (value)
        {
            case 0:
                prefix = "P1_";
                break;
            case 1:
                prefix = "P2_";
                break;
            case 2:
                prefix = "P3_";
                break;
            case 3:
                prefix = "P4_";
                break;
            case 4:
                prefix = "P5_";
                break;
            case 5:
                prefix = "P6_";
                break;
        }

        target.inputControllerPrefix = prefix;
    }
}
