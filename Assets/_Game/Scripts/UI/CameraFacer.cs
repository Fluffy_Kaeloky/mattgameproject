﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class CameraFacer : MonoBehaviour
{
    public Camera target = null;
    public bool invert = false;

    private void Start()
    {
        if (target == null)
            target = Camera.main;
    }

    private void Update()
    {
        transform.LookAt(invert ? -target.transform.position : target.transform.position);
    }
}
