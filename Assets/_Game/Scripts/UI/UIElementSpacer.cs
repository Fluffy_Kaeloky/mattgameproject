﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UIElementSpacer : MonoBehaviour
{
    public float space = 0.0f;
    public float slerpMultiplier = 10.0f;

    public Vector2 axis = new Vector2(1.0f, 0.0f);

    private void Update()
    {
        int count = 0;
        Vector3 oldChildrenPos = Vector3.zero;
        Vector2 oldChildrenSize = Vector2.zero;
        foreach (RectTransform children in transform)
        {
            children.localPosition = Vector3.Slerp(children.localPosition,
                oldChildrenPos + new Vector3((oldChildrenSize.x + space) * axis.x, (oldChildrenSize.y + space) * axis.y, 0.0f),
                Time.deltaTime * slerpMultiplier);

            oldChildrenSize = new Vector2(children.sizeDelta.x * children.localScale.x, children.sizeDelta.y * children.localScale.y);
            oldChildrenPos = children.localPosition;
            ++count;
        }
    }
}
