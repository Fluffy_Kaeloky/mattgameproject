﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class MainMenuController : MonoBehaviour
{
    public MainMenuAnimationControllerParameters animatorParameters = new MainMenuAnimationControllerParameters();
    public string sceneNameToLoad = "";

    private Animator animator = null;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void OnPlayButtonClick()
    {
        SceneManager.LoadScene(sceneNameToLoad);
    }

    public void OnOptionButtonClick()
    {
        animator.SetTrigger(animatorParameters.optionsName);
    }

    public void OnQuitButtonClick()
    {
        Application.Quit();
    }

    public void OnOptionsReturnButtonClick()
    {
        animator.SetTrigger(animatorParameters.mainName);
    }

    [System.Serializable]
    public class MainMenuAnimationControllerParameters
    {
        public string mainName = "Main";
        public string playName = "Play";
        public string optionsName = "Options";
        public string quitName = "Quit";
    }
}
