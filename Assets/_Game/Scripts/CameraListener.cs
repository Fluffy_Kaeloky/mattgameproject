﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(TopDownCamera))]
class CameraListener : MonoBehaviour
{
    private TopDownCamera topDownCamera = null;

    private void Awake()
    {
        topDownCamera = GetComponent<TopDownCamera>();
    }

    public void OnEnemySpawn(OnSpawnEnemyEventArgs args)
    {
        topDownCamera.Targets.Add(args.Sender.EnemyInfo.entity.transform);
    }

    public void OnEnemyDespawn(OnDespawnEnemyEventArgs args)
    {
        topDownCamera.Targets.Remove(args.Target.transform);
    }
}
