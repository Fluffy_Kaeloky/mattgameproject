﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Renderer))]
public class WavingAnimation : MonoBehaviour
{
    public List<int> materialsIndexes = new List<int>() { 0 };

    private new Renderer renderer = null;

    private void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    private void Update()
    {
        foreach (int materialIndex in materialsIndexes)
        {
            Vector4 waving = renderer.materials[materialIndex].GetVector("_WaveAndDistance");
            float wavingPower = waving.w;

            waving.x += Time.deltaTime * 0.1f * wavingPower * Random.value;
            waving.y -= Time.deltaTime * 0.1f * wavingPower * Random.value;

            renderer.materials[materialIndex].SetVector("_WaveAndDistance", waving);
        }
    }
}
