﻿using UnityEngine;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(Rigidbody))]
public class CameraSettingsTrigger : MonoBehaviour
{
    public string lookupTag = "PJ";

    public TopDownCamera cameraToApply = null;

    public CameraTopDownSettings settingsToApply = new CameraTopDownSettings();

    private CameraTopDownSettings oldSettings = new CameraTopDownSettings();

    private bool settingsApplied = false;

    private List<Collider> collidersInside = new List<Collider>();

    #region Private Methods

    private void Awake()
    {
        GetComponent<Rigidbody>().isKinematic = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        Entity ent = other.GetComponentInParent<Entity>();
        if (ent != null && ent.gameObject.tag == lookupTag)
        {
            if (!collidersInside.Contains(other))
                collidersInside.Add(other);

            if (settingsApplied)
                return;

            if (!CheckCameraReference())
                return;

            oldSettings = cameraToApply.topDownSettings;

            cameraToApply.topDownSettings.ApplySettings(settingsToApply);

            settingsApplied = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Entity ent = other.GetComponentInParent<Entity>();
        if (ent != null && ent.gameObject.tag == lookupTag)
        {
            if (collidersInside.Contains(other))
                collidersInside.Remove(other);

            if (!CheckCameraReference())
                return;

            if (collidersInside.Count != 0)
                return;

            cameraToApply.topDownSettings.ApplySettings(oldSettings);

            settingsApplied = false;
        }
    }

    private bool CheckCameraReference()
    {
        if (cameraToApply == null)
            cameraToApply = Camera.main.GetComponent<TopDownCamera>();

        return cameraToApply != null;
    }

    #endregion
}
