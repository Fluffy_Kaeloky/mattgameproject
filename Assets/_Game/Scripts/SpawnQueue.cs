﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class SpawnQueue : MonoBehaviour
{
    public UISpawnQueueEnemy prefab = null;

    public Transform parentTo = null;

    public OnSpawnEnemyEvent onEnemySpawn = new OnSpawnEnemyEvent();

    private List<UISpawnQueueEnemy> instances = new List<UISpawnQueueEnemy>();

    public void EnqueueSpawn(EnemyInfo spawn)
    {
        UISpawnQueueEnemy instance = Instantiate(prefab, parentTo, false) as UISpawnQueueEnemy;
        instance.transform.localPosition += new Vector3(1000.0f, 0.0f);

        instance.SetEnemyInfo(spawn);

        SpawnButtonCode spawnKey = SpawnButtonCode.A;
        for (int i = 0; i < 4; i++)
        {
            if (instances.Find(x => x.SpawnButton == (SpawnButtonCode.A + i)) == null)
            {
                spawnKey = SpawnButtonCode.A + i;
                break;
            }
        }

        instance.SetSpawnKey(spawnKey);
        instance.onDestroy.AddListener(OnSpawnEnemyQueueDestroy);

        instances.Add(instance);
    }

    public void Clear()
    {
        foreach (UISpawnQueueEnemy instance in instances)
            Destroy(instance.gameObject);

        instances.Clear();
    }

    private void Update()
    {
        for (int i = 0; i < 4; i++)
        {// SUPPRIME CE PUTAIN DE LIGNE DE CODE SI TU VEUX AVOIR LES CONTROLES SÉPARÉS
           // if (Input.GetKeyDown("joystick 1 button " + i))
            {
                //if (!PlayerCharInput.IsPrefixUsedAndEnabled("P1_"))
                   // RedistributeEvent("P1_", SpawnButtonCode.A + i);
            }
            if (Input.GetKeyDown("joystick 2 button " + i))
            {
                if (!PlayerCharInput.IsPrefixUsedAndEnabled("P2_"))
                    RedistributeEvent("P2_", SpawnButtonCode.A + i);
            }
            else if (Input.GetKeyDown("joystick 3 button " + i))
            {
                if (!PlayerCharInput.IsPrefixUsedAndEnabled("P3_"))
                    RedistributeEvent("P3_", SpawnButtonCode.A + i);
            }
            else if (Input.GetKeyDown("joystick 4 button " + i))
            {
                if (!PlayerCharInput.IsPrefixUsedAndEnabled("P4_"))
                    RedistributeEvent("P4_", SpawnButtonCode.A + i);
            }
        }
    }

    private void RedistributeEvent(string prefix, SpawnButtonCode button)
    {
        SpawnButtonPressedRedistrbutedEvent e = new SpawnButtonPressedRedistrbutedEvent(prefix, button);

        foreach (UISpawnQueueEnemy instance in instances)
        {
            instance.OnSpawnButtonEvent(e);
            if (e.IsAbsorbed)
                break;
        }
    }

    private void OnSpawnEnemyQueueDestroy(UISpawnQueueEnemy.OnSpawnQueueEmptyDestroyEventArgs args)
    {
        if (onEnemySpawn != null)
            onEnemySpawn.Invoke(new OnSpawnEnemyEventArgs(args.Sender));

        instances.Remove(args.Sender);
    }

    [System.Serializable]
    public struct SpawnButtonSpriteInfo
    {
        public SpawnButtonCode code;
        public Sprite image;
    }

    public enum SpawnButtonCode
    {
        A,
        B,
        X,
        Y
    }

    public class SpawnButtonPressedRedistrbutedEvent
    {
        private bool isAbsorbed = false;
        public bool IsAbsorbed { get { return isAbsorbed; } }

        private string inputPrefix = "";
        public string InputPrefix { get { return inputPrefix; } }

        private SpawnButtonCode keyCode = SpawnButtonCode.A;
        public SpawnButtonCode KeyCode { get { return keyCode; } }

        public SpawnButtonPressedRedistrbutedEvent(string prefix, SpawnButtonCode buttonPressed)
        {
            inputPrefix = prefix;
            keyCode = buttonPressed;
        }

        public void Absorb()
        {
            isAbsorbed = true;
        }
    }
}