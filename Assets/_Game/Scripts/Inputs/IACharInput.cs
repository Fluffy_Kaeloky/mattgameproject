﻿using UnityEngine;
using System.Collections;

public class IACharInput : BaseCharInput
{
    public float VerticalSpeed
    {
        get { return verticalSpeed; }
        set
        {
            verticalSpeed = Mathf.Clamp(value, -1.0f, 1.0f);
        }
    }

    public float HorizontalSpeed
    {
        get { return horizontalSpeed; }
        set
        {
            horizontalSpeed = Mathf.Clamp(value, -1.0f, 1.0f);
        }
    }

    public float TargetOrientation
    {
        get { return targetOrientation; }
        set
        {
            targetOrientation = value;
        }
    }

    public bool Attack1
    {
        get { return attack1; }
        set
        {
            attack1 = value;
        }
    }
}
