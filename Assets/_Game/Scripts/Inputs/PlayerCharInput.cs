﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

public class PlayerCharInput : BaseCharInput, IResetable
{
    public string inputControllerPrefix = "P1_";

    public string verticalInputName = "Vertical";
    public string VerticalInputName { get { return inputControllerPrefix + verticalInputName; } }
    public string horizontalInputName = "Horizontal";
    public string HorizontalInputName { get { return inputControllerPrefix + horizontalInputName; } }

    public string aimVerticalInputName = "RVerticalJoystick";
    public string AimVerticalInputName { get { return inputControllerPrefix + aimVerticalInputName; } }
    public string aimHorizontalInputName = "RHorizontalJoystick";
    public string AimHorizontalInputName { get { return inputControllerPrefix + aimHorizontalInputName; } }

    public string fire1InputName = "Fire1";
    public string Fire1InputName { get { return inputControllerPrefix + fire1InputName; } }

    public string rollInputName = "Roll";
    public string RollInputName { get { return inputControllerPrefix + rollInputName; } }

    public float rotationOffsetLerpFactor = 1.0f;

    private Coroutine fire1DisableCoroutine = null;

    private float rotationOffset = 0.0f;
    private float targetRotationOffset = 0.0f;

    private static List<PlayerCharInput> instances = new List<PlayerCharInput>();

    #region Public Methods

    public static bool IsPrefixUsedAndEnabled(string prefix)
    {
        foreach (PlayerCharInput input in instances)
        {
            if (input.gameObject.activeInHierarchy && input.enabled && input.inputControllerPrefix == prefix)
                return true;
        }

        return false;
    }

    public void OnReset()
    {
        enabled = true;
    }

    public void OnCameraSettingsChanged(TopDownCamera.TopDownCameraSettingsChangedEventArgs args)
    {
        Vector3 newCameraAxis = args.Sender.topDownSettings.Axis;

        newCameraAxis = Vector3.ProjectOnPlane(newCameraAxis, Vector3.up);

        Vector3 referenceAxis = Vector3.ProjectOnPlane(args.Sender.TopDownReferenceAxis, Vector3.up);

        Vector3 cross = Vector3.Cross(referenceAxis, Vector3.up);

        targetRotationOffset = Vector3.Angle(referenceAxis, newCameraAxis);
        if (Vector3.Dot(newCameraAxis, cross) <= 0.0f)
            targetRotationOffset = -targetRotationOffset;
    }

    #endregion

    #region Private Methods

    private void Awake()
    {
        instances.Add(this);
    }

    private void OnDestroy()
    {
        instances.Remove(this);
    }

    private void Update()
    {
        //Movement
        Vector3 movementAxis = new Vector3(Input.GetAxisRaw(HorizontalInputName), 0.0f, Input.GetAxisRaw(VerticalInputName));

        movementAxis = Quaternion.AngleAxis(-rotationOffset, Vector3.up) * movementAxis;

        verticalSpeed = movementAxis.z;
        horizontalSpeed = movementAxis.x;

        if (Input.GetButtonDown(RollInputName))
            roll = true;
        else
            roll = false;

        //Rotation Offset
        rotationOffset = Mathf.Lerp(rotationOffset, targetRotationOffset, rotationOffsetLerpFactor * Time.deltaTime);

        //Orientation
        Vector3 aimAxis = new Vector3(Input.GetAxisRaw(AimHorizontalInputName), 0.0f, Input.GetAxisRaw(AimVerticalInputName));

        aimAxis = Quaternion.AngleAxis(-rotationOffset, Vector3.up) * aimAxis;

        float verticalAimInput = aimAxis.z;
        float horizontalAimInput = aimAxis.x;

        Vector3 target = new Vector3(horizontalAimInput, 0.0f, verticalAimInput);
        if (target.magnitude > 0.5f)
        {
            Debug.DrawLine(transform.position, transform.position + target * 5.0f, Color.blue);

            float angle = Vector3.Angle(Vector3.forward, target);

            if (Vector3.Dot(target, Vector3.right) < 0.0f)
                angle = -angle;

            targetOrientation = angle;
        }

        //Attack
        if (attack1)
        {
            if (!Input.GetButton(Fire1InputName) && fire1DisableCoroutine == null)
                fire1DisableCoroutine = StartCoroutine(DisableFire1());
            else if (Input.GetButton(Fire1InputName) && fire1DisableCoroutine != null)
            {
                StopCoroutine(fire1DisableCoroutine);
                fire1DisableCoroutine = null;
            }
        }
        else
            attack1 = Input.GetButton(Fire1InputName);
    }

    private IEnumerator DisableFire1()
    {
        yield return new WaitForSeconds(0.2f);
        attack1 = false;
    }

    #endregion
}