﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharAnimationController))]
public abstract class BaseCharInput : MonoBehaviour
{
    protected float verticalSpeed = 0.0f;
    protected float horizontalSpeed = 0.0f;

    protected float targetOrientation = 0.0f;

    protected bool attack1 = false;

    protected bool roll = false;

    private CharAnimationController charAnimationController = null;

    private void Start()
    {
        charAnimationController = GetComponent<CharAnimationController>();
    }

    protected virtual void FixedUpdate()
    {
        charAnimationController.SetMovementSpeed(verticalSpeed, horizontalSpeed, Time.fixedDeltaTime);
        if (!charAnimationController.orientationFollowsVelocity)
            charAnimationController.SetTargetOrientation(targetOrientation, Time.fixedDeltaTime);

        charAnimationController.SetFire1State(attack1);
        charAnimationController.SetRollState(roll);
    }
}
