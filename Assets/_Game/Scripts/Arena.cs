﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Arena : MonoBehaviour
{
    public string lookupTag = "PJ";

    public int[] enemies = new int[0];
    public Transform[] spawnPositions = new Transform[0];

    public GameObject[] walls = new GameObject[0];

    private void Awake()
    {
        GetComponent<Rigidbody>().isKinematic = true;

        SetWallsState(false);

        if (enemies.Length == 0 || spawnPositions.Length == 0)
        {
            Debug.LogError(gameObject.name + "|Arena : incorrect configuration, please check your parameters.");
            enabled = false;
            return;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!enabled || BattleDirector.Instance.IsInBattle)
            return;

        Entity enteringEntity = other.GetComponentInParent<Entity>();
        if (enteringEntity == null)
            return;

        if (enteringEntity.gameObject.tag == lookupTag)
        {
            SetWallsState(true);
            BattleDirector.Instance.onBattleEnd.AddListener(OnBattleEnd);

            BattleDirector.Instance.StartBattle(enemies, spawnPositions);
        }
    }

    private void OnBattleEnd(BattleDirector.OnBattleEndEventArgs args)
    {
        SetWallsState(false);
        enabled = false;
    }

    private void SetWallsState(bool state)
    {
        foreach (GameObject w in walls)
            w.SetActive(state);
    }
}
