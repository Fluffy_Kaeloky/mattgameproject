﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class TopDownCamera : MonoBehaviour
{
    public float interpolationValue = 0.1f;
    public float lookAtInterpolationValue = 0.1f;

    [SerializeField]
    private List<Transform> targets = null;
    public List<Transform> Targets { get { return targets; } }

    public CameraTopDownSettings topDownSettings = new CameraTopDownSettings();
    public CameraCircularSettings circularSettings = new CameraCircularSettings();
    public CameraZoneLockedSettings zoneLockedSettings = new CameraZoneLockedSettings();

    public CameraTrackingMode WorkingMode { get { return workingMode; } set { workingMode = value; } }
    [SerializeField]
    private CameraTrackingMode workingMode = CameraTrackingMode.FollowTopDown;

    private Vector3 topDownReferenceAxis = Vector3.zero;
    public Vector3 TopDownReferenceAxis { get { return topDownReferenceAxis; } }

    public TopDownCameraSettingsChangedEvent onSettingsChanged = new TopDownCameraSettingsChangedEvent();

    private Vector3 oldCenterPosition = Vector3.zero;

    #region Public Methods

    public void SetTarget(Transform target)
    {
        SetTarget(target, CameraTrackingMode.FollowTopDown);
    }

    public void SetTarget(Transform target, CameraTrackingMode mode)
    {
        targets.Clear();
        this.targets.Add(target);
        workingMode = mode;
    }

    #endregion

    #region Private Methods

    private void Awake()
    {
        topDownSettings.onSettingsChanged += new EventHandler((sender, args) =>
        {
            if (onSettingsChanged != null)
                onSettingsChanged.Invoke(new TopDownCameraSettingsChangedEventArgs(this));
        }) ;
    }

    private void Start()
    {
        topDownReferenceAxis = topDownSettings.Axis;
    }

    private void LateUpdate()
    {
        switch (workingMode)
        {
            case CameraTrackingMode.FollowTopDown:
                UpdateTopDown();
                break;
            case CameraTrackingMode.ZoneLocked:
                UpdateZoneLocked();
                break;
            case CameraTrackingMode.Circular:
                UpdateCircular();
                break;
            default:
                break;
        }
    }

    private void UpdateCircular()
    {
        throw new NotImplementedException();
    }

    private void UpdateZoneLocked()
    {
        throw new NotImplementedException();
    }

    private void UpdateTopDown()
    {
        if (targets.Count() == 0)
            return;

        Vector3 minTarget = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
        Vector3 maxTarget = new Vector3(float.MinValue, float.MinValue, float.MinValue);

        foreach (Transform target in targets)
        {
            if (target.position.x < minTarget.x)
                minTarget.x = target.position.x;
            if (target.position.y < minTarget.y)
                minTarget.y = target.position.y;
            if (target.position.z < minTarget.z)
                minTarget.z = target.position.z;

            if (target.position.x > maxTarget.x)
                maxTarget.x = target.position.x;
            if (target.position.y > maxTarget.y)
                maxTarget.y = target.position.y;
            if (target.position.z > maxTarget.z)
                maxTarget.z = target.position.z;
        }

        Vector3 newCenterPosition = Vector3.Lerp(minTarget, maxTarget, 0.5f);

        float distance = Vector3.Distance(minTarget, maxTarget);
        if (distance == 0.0f)
            distance = topDownSettings.Distance;

        if (distance < topDownSettings.MinDistance)
            distance = topDownSettings.MinDistance;

        Vector3 targetPosition = Vector3.up * distance;
        targetPosition = Quaternion.AngleAxis(topDownSettings.Angle, topDownSettings.Axis) * targetPosition;

        targetPosition += newCenterPosition;

        transform.position = Vector3.Lerp(transform.position, targetPosition, interpolationValue * Time.deltaTime);

        oldCenterPosition = Vector3.Lerp(oldCenterPosition, newCenterPosition, lookAtInterpolationValue * Time.deltaTime);

        transform.LookAt(oldCenterPosition);
    }

    #endregion

    #region Classes Declarations
    [System.Serializable]
    public class TopDownCameraSettingsChangedEvent : UnityEvent<TopDownCameraSettingsChangedEventArgs> { }

    [System.Serializable]
    public class TopDownCameraSettingsChangedEventArgs
    {
        private TopDownCamera sender = null;
        public TopDownCamera Sender { get { return sender; } }

        public TopDownCameraSettingsChangedEventArgs(TopDownCamera sender)
        {
            this.sender = sender;
        }
    }

    #endregion
}

public enum CameraTrackingMode
{
    FollowTopDown,
    ZoneLocked,
    Circular
}

[System.Serializable]
public struct CameraTopDownSettings
{
    public event EventHandler onSettingsChanged;

    [SerializeField]
    private float angle;
    public float Angle { get { return angle; }
        set
        {
            angle = value;
            if (onSettingsChanged != null)
                onSettingsChanged.Invoke(this, new EventArgs());
        }
    }

    [SerializeField]
    private float distance;
    public float Distance { get { return distance; }
        set
        {
            distance = value;
            if (onSettingsChanged != null)
                onSettingsChanged.Invoke(this, new EventArgs());
        }
    }

    [SerializeField]
    private float minDistance;
    public float MinDistance { get { return minDistance; }
        set
        {
            minDistance = value;
            if (onSettingsChanged != null)
                onSettingsChanged.Invoke(this, new EventArgs());
        }
    }

    [SerializeField]
    private Vector3 axis;
    public Vector3 Axis { get { return axis; }
        set
        {
            axis = value;
            if (onSettingsChanged != null)
                onSettingsChanged.Invoke(this, new EventArgs());
        }
    }

    public void ApplySettings(CameraTopDownSettings settings)
    {
        angle = settings.angle;
        distance = settings.distance;
        minDistance = settings.minDistance;
        axis = settings.axis;

        if (onSettingsChanged != null)
            onSettingsChanged.Invoke(this, new EventArgs());
    }
}

[System.Serializable]
public struct CameraCircularSettings
{
    public float radius;
    public float angle;
}

[System.Serializable]
public struct CameraZoneLockedSettings
{
    public List<Transform> entities;
    public float maxDistance;
}

