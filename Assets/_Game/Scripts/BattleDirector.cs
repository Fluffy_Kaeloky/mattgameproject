﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Collections;

public class BattleDirector : MonoBehaviour
{
    public Transform[] spawns = new Transform[0];

    public EnemyInfo[] enemiesInfos = new EnemyInfo[0];

    public SpawnQueue spawnQueue = null;

    public AudioClip noBattleMusic = null;
    [Range(0.0f, 1.0f)]
    public float volumeBattleMusic = 1.0f;
    public AudioClip battleMusic = null;
    [Range(0.0f, 1.0f)]
    public float volumeNoBattleMusic = 0.7f;
    public float musicFadeTime = 1.0f;

    private bool isInBattle = false;
    public bool IsInBattle { get { return isInBattle; } }

    public bool OneOrMoreEnemiesActive {
        get
        {
            foreach (EnemyInfo e in enemiesInfos)
            {
                if (e.IsActive)
                    return true;
            }

            return false;
        }
    }

    public OnSpawnEnemyEvent onSpawnEnemy = new OnSpawnEnemyEvent();
    public OnDespawnEnemyEvent onDespawnEnemyEvent = new OnDespawnEnemyEvent();

    public OnBattleEndEvent onBattleEnd = new OnBattleEndEvent();

    private List<int> enemiesLeft = new List<int>();

    public static BattleDirector Instance { get { return instance; } }
    private static BattleDirector instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogError("There can be only one BattleDirector !");
            Destroy(this);
            return;
        }
    }

    private void Start()
    {
        spawnQueue.onEnemySpawn.AddListener(new UnityAction<OnSpawnEnemyEventArgs>(args => { if (onSpawnEnemy != null) onSpawnEnemy.Invoke(args); }));

        ResetAllEnemies();

        foreach (EnemyInfo e in enemiesInfos)
        {
            LifeManager lifeManager = e.entity.GetComponent<LifeManager>();
            if (!lifeManager)
            {
                Debug.LogError("Enemy in Batte director doesn't have a LifeManager ! Disabling script to prevent unexpected behaviour.");
                enabled = false;
                return;
            }

            lifeManager.onDeath.AddListener(OnEnemyDeath);
        }
    }

    public void StartBattle(int[] enemies, Transform[] spawnPositions)
    {
        Debug.Log("Starting battle");

        spawns = spawnPositions;

        enemiesLeft.Clear();
        enemiesLeft = new List<int>(enemies);

        ResetAllEnemies();

        SetEnemyAvailiable();

        isInBattle = true;

        if (battleMusic != null && AudioManager.Instance != null)
            AudioManager.Instance.FadeTo(battleMusic, musicFadeTime, volumeBattleMusic);
    }

    public void EndBattle()
    {
        Debug.Log("Battle has ended !");

        spawns = null;

        enemiesLeft.Clear();

        ResetAllEnemies();

        spawnQueue.Clear();

        isInBattle = false;

        if (onBattleEnd != null)
            onBattleEnd.Invoke(new OnBattleEndEventArgs());

        if (noBattleMusic != null && AudioManager.Instance != null)
            AudioManager.Instance.FadeTo(noBattleMusic, musicFadeTime, volumeNoBattleMusic);
    }

    public Transform GetRandomSpawn()
    {
        if (spawns == null || spawns.Length == 0)
            throw new System.Exception("No spawns defined in BattleDirector !");

        return spawns[Random.Range(0, spawns.Length)];
    }

    private EnemyInfo GetEnemy(int index)
    {
        foreach (EnemyInfo e in enemiesInfos)
        {
            if (e.identifier == index)
                return e;
        }

        throw new System.Exception("Could not find enemy index " + index + " !");
    }

    private void ResetAllEnemies()
    {
        foreach (EnemyInfo i in enemiesInfos)
            i.Reset();
    }

    private void OnEnemyDeath(OnDeathArgs args)
    {
        Entity dead = args.Sender.gameObject.GetComponent<Entity>();
        EnemyInfo enemyInfo = new EnemyInfo();

        foreach (EnemyInfo e in enemiesInfos)
        {
            if (e.entity == dead)
            {
                enemyInfo = e;
                break;
            }
        }

        StartCoroutine(EnemyDeathCoroutine(enemyInfo));
    }

    private IEnumerator EnemyDeathCoroutine(EnemyInfo dead)
    {
        yield return new WaitForSeconds(2.0f);

        if (onDespawnEnemyEvent != null)
            onDespawnEnemyEvent.Invoke(new OnDespawnEnemyEventArgs(dead.entity));

        dead.Reset();

        SetEnemyAvailiable();
    }

    private void SetEnemyAvailiable()
    {
        if (enemiesLeft.Count == 0 && !OneOrMoreEnemiesActive)
        {
            EndBattle();
            return;
        }

        EnemyInfo newEnemy;
        List<int> enemiesSpawned = new List<int>();
        foreach (int enemyIndex in enemiesLeft)
        {
            if (!(newEnemy = GetEnemy(enemyIndex)).IsActive)
            {
                enemiesSpawned.Add(enemyIndex);
                spawnQueue.EnqueueSpawn(newEnemy);

                newEnemy.IsActive = true;
            }
        }

        foreach (int i in enemiesSpawned)
            enemiesLeft.Remove(i);
    }

    [System.Serializable]
    public class OnBattleEndEvent : UnityEvent<OnBattleEndEventArgs> { }
    [System.Serializable]
    public class OnBattleEndEventArgs
    {
        //Ehhhhhh
    }
}

[System.Serializable]
public class EnemyInfo
{
    public int identifier;

    public Entity entity;
    public Sprite image;

    private bool isActive;
    public bool IsActive { get { return isActive; } set { isActive = value; } }

    public void Reset()
    {
        isActive = false;
        entity.gameObject.SetActive(false);
        entity.transform.position = Vector3.zero;

        entity.Reset();
    }
}
