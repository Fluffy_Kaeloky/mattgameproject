﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(IACharInput))]
[RequireComponent(typeof(CharAnimationController))]
public class EnemyIA : MonoBehaviour
{
    #region Members

    public string aiLayerName = "AI";

    public float sightMaxDistance = 10.0f;

    public float optimalTargetDistance = 10.0f;
    public float targetDistanceEpsilon = 5.0f;

    public float pathfindingCheckpointDistanceValidation = 0.5f;

    [Range(-1.0f, 1.0f)]
    public float firePrecision = 0.98f;

    [SerializeField]
    private Entity target = null;
    public Entity Target
    {
        get { return target; }
        set
        {
            target = value;
            RecalculatePath();
        }
    }

    public bool HasDirectViewOnTarget {
        get
        {
            if (target == null)
                return false;

            if (Vector3.Distance(transform.position, target.transform.position) > sightMaxDistance)
                return false;

            RaycastHit[] hits;
            Ray ray = new Ray(transform.position, target.transform.position - transform.position);

            LayerMask mask = LayerMask.NameToLayer(aiLayerName);
            mask = ~mask;
            hits = Physics.RaycastAll(ray, sightMaxDistance, mask.value, QueryTriggerInteraction.Ignore);

            RaycastHitDistanceComparer distanceComparer = new RaycastHitDistanceComparer();

            Array.Sort(hits, distanceComparer);

            Debug.DrawLine(transform.position, transform.position + ray.direction * sightMaxDistance, Color.red);

            if (hits.Length == 0)
                return false;

            foreach (RaycastHit hit in hits)
            {
                if (hit.transform.IsChildOf(transform))
                    continue;

                Entity ent = hit.transform.GetComponentInParent<Entity>();
                if (ent != null && ent == target)
                    return true;
                else
                    return false;
            }

            return false;
        }
    }

    private NavMeshPath path = null;
    private bool awake = false;

    private IACharInput charInput = null;

    private IAState state = IAState.Idle;

    private Queue<Vector3> pathCorners = new Queue<Vector3>();

    private CharAnimationController charAnimationController = null;

    #endregion

    #region Public Methods

    public void RecalculatePath()
    {
        if (target != null)
        {
            NavMesh.CalculatePath(transform.position, target.transform.position, NavMesh.AllAreas, path);
            pathCorners = new Queue<Vector3>(path.corners);
            pathCorners.Dequeue();
        }
        else
        {
            path = null;
            pathCorners = new Queue<Vector3>();
        }
    }

    #endregion

    #region Private Methods

    private void Awake()
    {
        charInput = GetComponent<IACharInput>();
        charAnimationController = GetComponent<CharAnimationController>();
    }

    private void Start()
    {
        path = new NavMeshPath();

        if (target != null)
            RecalculatePath();
    }

    private void FixedUpdate()
    {
        charInput.Attack1 = false;
        
        if (target == null)
            return;

        bool hasDirectView = HasDirectViewOnTarget;

        if (!awake)
        {
            charInput.VerticalSpeed = 0.0f;
            charInput.HorizontalSpeed = 0.0f;

            if (hasDirectView)
                awake = true;

            return;
        }

        if (!hasDirectView)
        {
            if (path == null || pathCorners == null || pathCorners.Count == 0 || target.transform.hasChanged)
            {
                RecalculatePath();
                target.transform.hasChanged = false;
            }

            if (pathCorners.Count == 0)
                return;

            if (Vector3.Distance(transform.position, pathCorners.Peek()) <= pathfindingCheckpointDistanceValidation)
            {
                pathCorners.Dequeue();
                if (pathCorners.Count == 0)
                    return;
            }

            for (int i = 0; i < pathCorners.Count - 1; i++)
                Debug.DrawLine(pathCorners.ToArray()[i], pathCorners.ToArray()[i + 1], Color.yellow);

            DebugExtension.DebugWireSphere(pathCorners.Peek(), Color.red, 2.0f);
            Vector3 lookAtDirection = (pathCorners.Peek() - transform.position).normalized;
            Debug.DrawLine(transform.position, transform.position + (lookAtDirection * 5.0f), Color.magenta);

            //Orientation
            float angle = Vector3.Angle(Vector3.forward, lookAtDirection);
            if (Vector3.Dot(lookAtDirection, Vector3.right) < 0.0f)
                angle = -angle;

            charInput.TargetOrientation = angle;

            //Movement
            Vector3 movementDirection = Vector3.forward;
            movementDirection = transform.TransformDirection(movementDirection);

            charInput.VerticalSpeed = movementDirection.z;
            charInput.HorizontalSpeed = movementDirection.x;

            return;
        }

        switch (state)
        {
            case IAState.Moving:
                HandleMovingState();
                break;
            case IAState.Idle:
                HandleIdleState();
                break;
            default:
                break;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, sightMaxDistance);
    }

    private void HandleMovingState()
    {
        Vector3 lookAtDirection = Vector3.zero;

        float currentDistance = Vector3.Distance(transform.position, target.transform.position);

        float diff = currentDistance - optimalTargetDistance;

        //Hard coded limit
        if (Mathf.Abs(diff) > (targetDistanceEpsilon - (targetDistanceEpsilon / 2.0f)))
        {
            //We're too far
            if (diff > 0.0f)
                lookAtDirection = Vector3.ProjectOnPlane((target.transform.position - transform.position), Vector3.up).normalized;

            //We're too close
            if (diff < 0.0f)
                lookAtDirection = -Vector3.ProjectOnPlane((target.transform.position - transform.position), Vector3.up).normalized;
        }
        else
            state = IAState.Idle;

        //Orientation
        float angle = Vector3.Angle(Vector3.forward, lookAtDirection);
        if (Vector3.Dot(lookAtDirection, Vector3.right) < 0.0f)
            angle = -angle;

        charInput.TargetOrientation = angle;

        //Movement
        Vector3 movementDirection = Vector3.forward;
        movementDirection = transform.TransformDirection(movementDirection);

        charInput.VerticalSpeed = movementDirection.z;
        charInput.HorizontalSpeed = movementDirection.x;
    }

    private void HandleIdleState()
    {
        Vector3 lookAtDirection = (target.transform.position - transform.position).normalized;

        //Orientation
        float angle = Vector3.Angle(Vector3.forward, lookAtDirection);
        if (Vector3.Dot(lookAtDirection, Vector3.right) < 0.0f)
            angle = -angle;

        charInput.TargetOrientation = angle;

        //Movement
        charInput.VerticalSpeed = 0.0f;
        charInput.HorizontalSpeed = 0.0f;

        //State change
        float currentDistance = Vector3.Distance(transform.position, target.transform.position);

        float diff = currentDistance - optimalTargetDistance;

        //Hard coded limit
        if (Mathf.Abs(diff) > targetDistanceEpsilon)
            state = IAState.Moving;

        //Fire
        Vector3 forward = Quaternion.AngleAxis(-charAnimationController.angleOffset, Vector3.up) * transform.forward;

        Debug.DrawLine(transform.position, transform.position + forward * 5.0f, Color.green);

        if (Vector3.Dot(forward, lookAtDirection) >= firePrecision)
            charInput.Attack1 = true;
    }

    #endregion

    #region Classes Declarations

    public enum IAState
    {
        Moving,
        Idle
    }

    #endregion
}
