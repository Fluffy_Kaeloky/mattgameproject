﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip defaultMusic = null;
    [Range(0.0f, 1.0f)]
    public float volume = 1.0f;

    private static AudioManager instance = null;
    public static AudioManager Instance { get { return instance; } }

    private Transform sfxContainer = null;

    private AudioSource audioSource = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);

            GameObject go = new GameObject();
            go.transform.SetParent(transform, false);

            sfxContainer = go.transform;

            audioSource = sfxContainer.gameObject.AddComponent<AudioSource>();
            audioSource.loop = true;
        }
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        if (defaultMusic != null)
            audioSource.clip = defaultMusic;

        audioSource.Play();
    }

    private void Update()
    {
        if (Camera.main != null)
            sfxContainer.transform.position = Camera.main.transform.position;
    }

    public void FadeTo(AudioClip clip, float fadeTime = 1.0f, float volumeMultiplier = 1.0f)
    {
        StartCoroutine(FadeOutAndChangeClip(clip, fadeTime, volumeMultiplier));
    }

    private IEnumerator FadeOutAndChangeClip(AudioClip newClip, float fadeTime, float volumeMultiplier)
    {
        float time = 0.0f;
        while (true)
        {
            float value = time / fadeTime;

            if (value < 0.5f)
            {
                audioSource.volume = ((0.5f - value) / 0.5f) * volume * volumeMultiplier;
            }
            else if (value > 0.5f && value < 1.0f)
            {
                if (audioSource.clip != newClip)
                {
                    audioSource.clip = newClip;
                    audioSource.Play();
                }

                audioSource.volume = ((value - 0.5f) / 0.5f) * volume * volumeMultiplier;
            }
            else
                break;

            time += Time.deltaTime;

            yield return null;
        }
    }
}
