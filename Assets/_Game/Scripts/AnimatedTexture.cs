﻿using UnityEngine;
using System.Collections;

public class AnimatedTexture : MonoBehaviour
{
    public int columnsCount = 1;
    public int rowsCount = 1;

    public float columnSize = 1.0f;
    public float rowSize = 1.0f;

    public float switchTime = 0.5f;

    public int materialIndex = 0;

    private float startX = 0.0f;
    private float startY = 0.0f;

    private int currentRow = 0;
    private int currentColumn = 0;

    private new Renderer renderer = null;

    private Coroutine routine = null;

    private void Start()
    {
        renderer = GetComponent<Renderer>();

        startX = renderer.materials[materialIndex].mainTextureOffset.x;
        startY = renderer.materials[materialIndex].mainTextureOffset.y;

        if (renderer == null)
        {
            Debug.LogError("No renderer attached to this gameObject !");
            enabled = false;
            return;
        }

        StartUpdate();
    }

    private void OnEnable()
    {
        StartUpdate();
    }

    private void OnDisable()
    {
        StopUpdate();
    }

    private void StartUpdate()
    {
        StopUpdate();

        routine = StartCoroutine(UpdateTexture());
    }

    private void StopUpdate()
    {
        if (routine != null)
            StopCoroutine(routine);
    }

    private IEnumerator UpdateTexture()
    {
        yield return null;

        while (true)
        {
            renderer.materials[materialIndex].mainTextureOffset = new Vector2(startX + columnSize * currentColumn, startY + rowSize * currentRow);

            if (++currentColumn >= columnsCount)
            {
                currentColumn = 0;
                if (++currentRow >= rowsCount)
                    currentRow = 0;
            }

            yield return new WaitForSeconds(switchTime);
        }
    }
}
