﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class LifeManager : MonoBehaviour, IResetable
{
    public OnDamageTaken onDamageTaken = new OnDamageTaken();
    public OnDeath onDeath = new OnDeath();

    public OnLifeChanged onLifeChanged = new OnLifeChanged();

    [SerializeField]
    private float maxLife = 100.0f;
    public float MaxLife { get { return maxLife; } set
        {
            maxLife = value;
            if (onLifeChanged != null)
                onLifeChanged.Invoke(new OnLifeChangedArgs(this, life));
        }
    }


    [SerializeField]
    private float life = 100.0f;
    public float Life { get { return life; } set
        {
            life = value;
            if (onLifeChanged != null)
                onLifeChanged.Invoke(new OnLifeChangedArgs(this, life));
        }
    }

    public bool IsDead { get { return life <= 0.0f; } }

    public void TakeDamage(/*Entity instigator,*/ float amount)
    {
        if (IsDead)
            return;

        float newLife = life - amount;
        if (newLife <= 0.0f)
            newLife = 0.0f;

        Life = newLife;

        if (onDamageTaken != null)
            onDamageTaken.Invoke(new OnDamageTakenArgs(this, /*instigator*/null, amount));

        if (IsDead && onDeath != null)
            onDeath.Invoke(new OnDeathArgs(this, /*instigator*/null));
    }

    public void OnReset()
    {
        life = maxLife;
        if (onLifeChanged != null)
            onLifeChanged.Invoke(new OnLifeChangedArgs(this, life));
    }
}

[System.Serializable]
public class OnDamageTaken : UnityEvent<OnDamageTakenArgs> { }

[System.Serializable]
public class OnDamageTakenArgs
{
    private LifeManager sender = null;
    public LifeManager Sender { get { return sender; } }

    public Entity Instigator { get { return instigator; } }
    private Entity instigator = null;

    public float DamageAmount { get { return damageAmount; } }
    private float damageAmount = 0.0f;

    public OnDamageTakenArgs(LifeManager sender, Entity instigator, float damageAmount)
    {
        this.sender = sender;
        this.instigator = instigator;
        this.damageAmount = damageAmount;
    }
}

[System.Serializable]
public class OnDeath : UnityEvent<OnDeathArgs> { }

[System.Serializable]
public class OnDeathArgs
{
    private LifeManager sender = null;
    public LifeManager Sender { get { return sender; } }

    public Entity Instigator { get { return instigator; } }
    private Entity instigator = null;

    public OnDeathArgs(LifeManager sender, Entity instigator)
    {
        this.sender = sender;
        this.instigator = instigator;
    }
}

[System.Serializable]
public class OnLifeChanged : UnityEvent<OnLifeChangedArgs> { }

[System.Serializable]
public class OnLifeChangedArgs
{
    private LifeManager sender = null;
    public LifeManager Sender { get { return sender; } }

    public float NewAmount { get { return newAmount; } }
    private float newAmount = 0.0f;

    public OnLifeChangedArgs(LifeManager sender, float newAmount)
    {
        this.sender = sender;
        this.newAmount = newAmount;
    }
}