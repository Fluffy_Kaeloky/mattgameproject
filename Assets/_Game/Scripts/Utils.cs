﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class RaycastHitDistanceComparer : IComparer
{
    public int Compare(object x, object y)
    {
        RaycastHit a = (RaycastHit)x;
        RaycastHit b = (RaycastHit)y;

        return (int)Mathf.Sign(a.distance - b.distance);
    }
}