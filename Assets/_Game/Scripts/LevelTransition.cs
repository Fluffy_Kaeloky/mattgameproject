﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class LevelTransition : MonoBehaviour
{
    public string levelName = "";

    public string tagFilter = "PJ";

    private void Awake()
    {
        GetComponent<Rigidbody>().isKinematic = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        Entity enteringEntity = other.GetComponentInParent<Entity>();
        if (enteringEntity != null && enteringEntity.gameObject.tag == tagFilter)
            SceneManager.LoadScene(levelName);
    }
}
