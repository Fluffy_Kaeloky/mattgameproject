﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(Entity))]
[RequireComponent(typeof(EnemyIA))]
public class DroneWeapon : MonoBehaviour
{
    #region Members

    public DroneLaser prefab = null;
    public Transform fireMuzzle = null;

    private EnemyIA enemyAI = null;
    private Entity entity = null;

    #endregion

    #region Public Methods

    public void Fire()
    {
        DroneLaser laser = Instantiate(prefab, fireMuzzle, false) as DroneLaser;

        laser.Direction = enemyAI.Target != null ? (enemyAI.Target.transform.position - fireMuzzle.position) : fireMuzzle.forward;

        Debug.DrawLine(transform.position, transform.position + laser.Direction, Color.black, 5.0f);

        laser.owner = entity.transform;
    }

    #endregion

    #region Private Methods

    private void Start()
    {
        enemyAI = GetComponent<EnemyIA>();
        entity = GetComponent<Entity>();
    }

    #endregion
}
