﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharAnimationController : MonoBehaviour, IResetable
{
    #region Members

    public float movementDampTime = 0.1f;
    public float orientationDampTime = 0.1f;

    public bool orientationFollowsVelocity = true;

    public float angleOffset = 0.0f;

    public bool OrientationLocked { get { return orientationLocked; } set { orientationLocked = value; } }
    private bool orientationLocked = false;

    [SerializeField]
    private AnimationParametersNames animationParametersNames = new AnimationParametersNames();

    private Animator animator = null;

    #endregion

    #region Public Methods

    public void SetMovementSpeed(float vertical, float horizontal, float deltaTime)
    {
        Vector3 dir = new Vector3(horizontal, 0.0f, vertical);

        //Orientation DOES NOT follow velocity
        if (!orientationFollowsVelocity)
        {
            dir = transform.InverseTransformDirection(dir);

            if (!string.IsNullOrEmpty(animationParametersNames.verticalSpeedName))
                animator.SetFloat(animationParametersNames.verticalSpeedName, dir.z, movementDampTime, deltaTime);
            if (!string.IsNullOrEmpty(animationParametersNames.horizontalSpeedName))
                animator.SetFloat(animationParametersNames.horizontalSpeedName, dir.x, movementDampTime, deltaTime);
            return;
        }

        //Orientation follows velocity
        dir = dir.normalized;

        Debug.DrawLine(transform.position, transform.position + dir * 5.0f, Color.red);
        float angle = Vector3.Angle(Vector3.forward, dir);

        if (Vector3.Dot(dir, Vector3.right) < 0.0f)
            angle = -angle;

        if (dir.magnitude != 0.0f)
            SetTargetOrientation(angle, deltaTime);

        float absVertical = Mathf.Abs(vertical);
        float absHorizontal = Mathf.Abs(horizontal);

        float multiplier = Mathf.Max(absVertical, absHorizontal);

        if (!string.IsNullOrEmpty(animationParametersNames.verticalSpeedName))
            animator.SetFloat(animationParametersNames.verticalSpeedName,
                Mathf.Max(multiplier * Vector3.Dot(Vector3.ProjectOnPlane(transform.forward, Vector3.up).normalized, dir), 0.0f),
                movementDampTime, deltaTime);

        if (!string.IsNullOrEmpty(animationParametersNames.horizontalSpeedName))
            animator.SetFloat(animationParametersNames.horizontalSpeedName, 0.0f, movementDampTime, deltaTime);
    }

    public void SetTargetOrientation(float angle, float deltaTime)
    {
        if (orientationLocked)
            return;

        Quaternion q = Quaternion.AngleAxis(angle + angleOffset, Vector3.up);

        transform.rotation = Quaternion.Slerp(transform.rotation, q, orientationDampTime * deltaTime);
    }

    public void SetFire1State(bool state)
    {
        if (!string.IsNullOrEmpty(animationParametersNames.fire1Name))
            animator.SetBool(animationParametersNames.fire1Name, state);
    }

    public void SetRollState(bool state)
    {
        if (!string.IsNullOrEmpty(animationParametersNames.rollName))
            animator.SetBool(animationParametersNames.rollName, state);
    }

    public void Kill()
    {
        if (!string.IsNullOrEmpty(animationParametersNames.deathName))
            animator.SetTrigger(animationParametersNames.deathName);
    }

    public void OnReset()
    {
        if (!string.IsNullOrEmpty(animationParametersNames.resetName) && animator.isInitialized)
            animator.SetTrigger(animationParametersNames.resetName);
    }

    public void LockOrientation()
    {
        OrientationLocked = true;
    }

    public void UnlockOrientation()
    {
        OrientationLocked = false;
    }

    #endregion

    #region Private Methods

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    #endregion

    #region Classes Declarations

    [System.Serializable]
    public class AnimationParametersNames
    {
        public string verticalSpeedName = "VerticalSpeed";
        public string horizontalSpeedName = "HorizontalSpeed";
        public string fire1Name = "Attack1";
        public string fire2Name = "Attack2";
        public string deathName = "Death";
        public string resetName = "Reset";
        public string rollName = "Roll";
    }

    #endregion
}
