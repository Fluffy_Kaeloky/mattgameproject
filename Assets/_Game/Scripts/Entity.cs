﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public string EntityName { get { return entityName; } }
    [SerializeField]
    private string entityName = "";

    public void Reset()
    {
        IResetable[] resetables = GetComponentsInChildren<IResetable>();

        foreach (IResetable r in resetables)
            r.OnReset();
    }
}
