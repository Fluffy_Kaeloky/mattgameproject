﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory(ActionCategory.UnityObject)]
    [Tooltip("Change le texte d'un TextMesh, avec un string, un int ou un float")]
    public class SetTextMesh : ComponentAction<TextMesh>
    {
        [RequiredField]
        [CheckForComponent(typeof(TextMesh))]
        public FsmOwnerDefault gameObject;

        public FsmString StringToWrite;
        public FsmFloat FloatToWrite;
        public FsmInt IntToWrite;


        public bool everyFrame;

        public override void Reset()
        {
            gameObject = null;
            StringToWrite = new FsmString { UseVariable = true };
            FloatToWrite = new FsmFloat { UseVariable = true };
            IntToWrite = new FsmInt { UseVariable = true };

            everyFrame = false;
        }

        public override void OnEnter()
        {
            DoSetTextMesh();

            if (!everyFrame)
            {
                Finish();
            }
        }
        public override void OnUpdate()
        {
            DoSetTextMesh();

            if (!everyFrame)
            {
                Finish();
            }
        }
        void DoSetTextMesh()
        {
            var go = Fsm.GetOwnerDefaultTarget(gameObject);
            if (!UpdateCache(go))
            {
                return;
            }

            string toWrite = "";
            if (!StringToWrite.IsNone)
                toWrite = StringToWrite.Value;
            if (!FloatToWrite.IsNone)
                toWrite = (Mathf.Floor(FloatToWrite.Value * 100) / 100).ToString();
            if (!IntToWrite.IsNone)
                toWrite = IntToWrite.Value.ToString();

            go.GetComponent<TextMesh>().text = toWrite;
        }

    }
}
